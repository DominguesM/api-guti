# Guia para a submissão de Merge Requests
Este documento contém um [template](#template) em branco que deve ser preenchido a cada Merge Request submetido ao projeto.

## Regras de preenchimento do template

As seções do template devem ser preenchidas com as seguintes informações respectivamente:

- Descrição das mudanças contidas no Merge Request;
- Passos e dicas para que quem for realizar o code review consiga testar de maneira bem-sucedida as alterações realizadas. É necessário que todas as pré-condições para a execução dos testes sejam elencadas aqui.
- Passos para testar os casos de sucesso dos pontos de alteração.
- Passos para testar os casos de exceção dos pontos de alteração.
- Comentários gerais sobre as suas mudanças e decisões de arquitetura. Esse é um bom espaço para colocar links e fundamentar suas alterações, bem como sanar possíveis dúvidas de quem for corrigir seu Merge Request.

#### Observações:
- Salvo a seção de comentários, todas as seções devem ser criadas utilizando-se de tópicos (bullet points); 
- Caso alguma seção não se aplique ao seu Merge Request, você deve preencher a mesma com **`N/A`**.


## Template

**Descrição das mudanças:**


**Setup do ambiente para testes:**


**Como testar o caso de sucesso:**


**Como testar o caso de erro:**


**Comentários:**


## Exemplo de Merge Request Preenchido

[Este Merge Request](https://gitlab.com/api-projects-boilerplates/api-pj-movimentos/merge_requests/126) pode ser utilizado como guia caso existam dúvidas na formatação ou conteúdo de cada uma das seções do template.
