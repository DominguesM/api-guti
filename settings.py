# -*- coding: utf-8 -*-

"""Global settings for the project"""

import os

from tornado.options import define

port = os.environ.get("PORT", 8000)

define("port", default=port, help="run on the given port", type=int)
define("config", default=None, help="tornado config file")
define("debug", default=False, help="debug mode")

__BASE_PACKAGE__ = "api"

settings = dict()

settings["debug"] = True
settings["cookie_secret"] = "QhSDKovSedracpOAYWvdVAK8U"
settings["static_path"] = os.path.join(
    os.path.dirname(__file__), __BASE_PACKAGE__, "static"
)
settings["template_path"] = os.path.join(
    os.path.dirname(__file__), __BASE_PACKAGE__, "templates"
)
settings["xsrf_cookies"] = False
