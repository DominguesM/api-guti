
install:
	@echo "--> Installing requirements"
	pip install -r requirements-dev.txt
	@echo "--> Installing black"
	pip install black
	@echo "--> Installing pre-commit"
	pip install pre-commit
	pre-commit install
	@echo ""

lint:
	@echo "--> Running flake8"
	flake8 iarapis/*
	@echo ""
	@echo "--> Running black"
	black --check iarapis/*
	@echo "--> Running pylint"
	pylint iarapis/* --rcfile=setup.cfg --jobs=4
	@echo ""

safety:
	@echo "--> Running safety"
	safety check
	@echo ""

test:
	@echo "--> Running tests"
	pytest iarapis -vv -s --cov-fail-under 80 --cov=./iarapis --cov-report xml:coverage/coverage.xml --cov-report html:coverage/coverage_html --cov-report term-missing .
	@echo ""

clean:
	@echo "--> Removing .pyc and __pycache__"
	find . | grep -E "__pycache__|.pyc$$" | xargs rm -rf
	@echo ""
	@echo "--> Removing coverage data"
	rm -rf .coverage* .cache htmlcov
	rm -rf ./coverage
	@echo ""

lint-fix:
	@echo "--> Auto fix lint errors "
	autopep8 --in-place --recursive .
	black .
	@echo ""

run:
	@echo "--> Running"
	python run.py