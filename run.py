# coding:utf-8
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.log import enable_pretty_logging
from tornado.options import options

from settings import settings
from iarapis.urls import url_patterns


class Default404Handler(tornado.web.RequestHandler):
    def prepare(self):
        self.set_header("Content-Type", "application/json")
        self.set_status(404)
        self.write({"status": 404, "data": "Nothing matches the given URI"})
        self.finish()


def make_app(url_patterns):
    return tornado.web.Application(
        url_patterns, default_handler_class=Default404Handler, **settings
    )


def main():
    tornado.options.parse_command_line()
    enable_pretty_logging()

    app = make_app(url_patterns)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    http_server.start(0)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
        exit(0)
