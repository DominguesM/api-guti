FROM python:3.7-stretch

RUN mkdir /app
RUN apt-get update && apt-get install git

WORKDIR /app
COPY ./ ./

RUN pip install six
RUN pip install -r ./requirements.txt

RUN python -m spacy download pt

CMD ["python", "run.py"]

EXPOSE 8000
