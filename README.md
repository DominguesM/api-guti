# API

## Execução

```sh
$ docker-compose up --build
```

A API será exposta na porta **80**

## Rotas

### /tools/v1.0/summarization

**Paramêtros:**

```json
{
    "text": "Texto a ser resumido [STRING - OBRIGATÓRIO]",
    "ratio": "Porcentagem do texto ser retornada [FLOAT - OPCIONAL - PADRÃO: 0.05 (5%)]"
}

```

**Retorno:**

```json
{
    "status": 200,
    "data": {
        "summarization": "dias toffoli julgamento: 12/09/2019 publicacao processo eletronico dje-204 divulg 19/09/2019 public 20/09/2019 partes recte.(s) : estado do parana proc.(a/s)(es) : procurador-geral do estado do parana recdo.(a/s) : caixa economica federal - cef adv.(a/s) : clevis vasquinho lapinski decisao decisao: trata-se de recurso extraordinario interposto com base no art.",
        "html_highlight": "20/09/2019 pesquisa de jurisprudencia :: stf - supremo tribunal federal pesquisa de jurisprudencia decisoes da presidencia re 1232060 / pr - parana recurso extraordinario relator(a): min. <mark>dias toffoli julgamento: 12/09/2019 publicacao processo eletronico dje-204 divulg 19/09/2019 public 20/09/2019 partes recte.(s) : estado do parana proc.(a/s)(es) : procurador-geral do estado do parana recdo.(a/s) : caixa economica federal - cef adv.(a/s) : clevis vasquinho lapinski decisao decisao: trata-se de recurso extraordinario interposto com base no art.</mark> 102, inciso iii, da constituicao federal. decido. analisados os autos, verifica- se que o recurso especial interposto simultaneamente ao recurso extraordinario foi provido pelo superior tribunal de justica, que atendeu a pretensao do recorrente. o apelo extremo, portanto, esta prejudicado, tendo em vista a perda superveniente de seu objeto, decorrente da substituicao do julgado (art. 1.008 do codigo de processo civil). nesse sentido: agravo regimental no recurso extraordinario. recurso especial provido pelo superior tribunal de justica para anular acordao dos embargos de declaracao da corte de origem. recurso extraordinario prejudicado. precedentes. 1. o provimento do recurso especial pelo superior tribunal de justica, anulando-se o acordao dos embargos de declaracao e determinando-se a realizacao de novo julgamento pela corte de origem, torna prejudicado o recurso extraordinario, por perda de objeto. 2. agravo regimental nao provido, com imposicao de multa de 1% do valor atualizado da causa (art. 1.021,  4o, do cpc (re no 1.113.783/ma agr, plenario, rel. min. dias toffoli, dje de 20/11/18). agravo regimental nos embargos de declaracao no recursoextraordinario. processual civil. recurso especial provido: denegacao do mandado de seguranca. recurso extraordinario prejudicado: perda superveniente de objeto. multa aplicada no percentual de 1%, conforme o  4o do art. 1.021 do codigo de processo civil. agravo regimental ao qual se nega provimento (re no 1.069.871/rsedagr, plenario, rel. min. carmen lucia, dje de 26/6/18). pelo exposto, julgo prejudicado o presente recurso pela perda do objeto (al. c do inc. v do art. 13 do regimento interno do supremo tribunal federal). publique-se. brasilia, 12 de setembro de 2019. ministro dias toffoli presidente documento assinado digitalmente fim do documento stf.jus.br/portal/jurisprudencia/listarjurisprudenciadetalhe.asp?s1=000438533&base=basepresidencia 1/1",
        "topics": [
            "federal",
            "prejudicado",
            "parana recurso extraordinario",
            "jurisprudencia",
            "civil",
            "objeto",
            "art",
            "provido pelo",
            "stf",
            "dje",
            "justica",
            "plenario",
            "decisao",
            "julgamento",
            "agravo",
            "toffoli",
            "interposto com",
            "perda",
            "pela",
            "especial"
        ]
    }
}
```

O retorno é composto pelo texto resumido, o texto original com highlight e uma lista dos principais tópicos do texto.

### /tools/v1.0/entities

**Paramêtros:**

```json
{
    "text": "Texto para extração de entidades[STRING - OBRIGATÓRIO]"
}

```

**Retorno:**

```json
{
    "status": 200,
    "data": {
        "entities": [
            {
                "label": "LOC",
                "value": "stf"
            },
            {
                "label": "LOC",
                "value": "supremo tribunal federal"
            },
            {
                "label": "PER",
                "value": "dias toffoli"
            },
            {
                "label": "PER",
                "value": "divulg"
            },
            {
                "label": "LOC",
                "value": "parana proc.(a"
            },
            {
                "label": "LOC",
                "value": "parana recdo.(a"
            },
            {
                "label": "ORG",
                "value": "caixa economica federal"
            },
            {
                "label": "MISC",
                "value": "cef adv.(a"
            },
            {
                "label": "PER",
                "value": "inciso iii"
            },
            {
                "label": "LOC",
                "value": "constituicao federal"
            },
            {
                "label": "PER",
                "value": "decido"
            },
            {
                "label": "LOC",
                "value": "substituicao do julgado"
            },
            {
                "label": "ORG",
                "value": "cpc"
            },
            {
                "label": "PER",
                "value": "ma agr"
            },
            {
                "label": "LOC",
                "value": "plenario"
            },
            {
                "label": "LOC",
                "value": "rel"
            },
            {
                "label": "PER",
                "value": "min"
            },
            {
                "label": "PER",
                "value": "dias toffoli"
            },
            {
                "label": "MISC",
                "value": "dje de 20/11/18"
            },
            {
                "label": "PER",
                "value": "seguranca"
            },
            {
                "label": "PER",
                "value": "min"
            },
            {
                "label": "MISC",
                "value": "carmen"
            },
            {
                "label": "MISC",
                "value": "dje de 26/6/18"
            },
            {
                "label": "MISC",
                "value": "al"
            },
            {
                "label": "MISC",
                "value": "c"
            },
            {
                "label": "ORG",
                "value": "inc"
            },
            {
                "label": "MISC",
                "value": "v do art"
            },
            {
                "label": "LOC",
                "value": "supremo tribunal federal"
            },
            {
                "label": "LOC",
                "value": "brasilia"
            },
            {
                "label": "PER",
                "value": "ministro dias toffoli"
            }
        ]
    }
}
```

### /tools/v1.0/wordcloud

**Paramêtros:**

```json
{
    "text": "Texto [STRING - OBRIGATÓRIO]",
}

```

**Retorno:**

```json
{
    "status": 200,
    "data": {
        "img_wc": "iVBORw0KGgoAAAANSUhEUgAAAZAAAADICAIAAABJdyC1AAC6lUlEQVR4nOydZ3xb5dnwrzO197Al772z994BQsIKM2W1tKWlmw7aQp++Ld2lpaUtXUDL3hAgEEJ2yN6248R7y5Zk7Xn2+0GOLMuyLM8kNP+fPkj3udcZus49rgFwlatc5SpXCMil7sBVJgwUJ4rufaRr+8uB7uapaRGTyZWz5slKykVGEyIS8VSY6un2HD3or6uOyykyZWhXrJPk5mMSKSCDnrqW3/wf5/cBAGkw5nzjkb5Ptrn271LNWaiav5jUGQSWpWw9tvffoq09KTaKYFje93+KiSWtv/8Z6/MO6geK5n/v/zCZvPWJx1mPK5JGqLWaFWtkhSWYXMmHgsHWZue+T6LNXSVFCswrSVzS2nswTHsmrxV88qq+tJDZ5rRHvkQ1d9qeeGbMlRAmg/mXD0d/Bk/X2f/8/ET0bpJAAEDg2SlrTz1/iXblOj4coqw9nM9LaPXS/CJpfpH1rZe9Z05Es4nMmVlf/AYIgufEEdbjEmflyiumAYBj54dUTzcfCsbWicuVhmtvVM1bFOpo9duthFYvycrlYuTOiI0KHOc7e0q9cKli2kzXwX2xlUvzizG5ItjSGJVW4qycjHu+jIrF4a6OUHsrrlQpKqbJyyp7Xvtv4MK5cV2cyrnu2uPjqeEKgiTkBeZlAEi348xVgTVWBEEIU+OpgHV6+v71OiaXknmZsgUzJqhbkwXP0vXP/HwqW3Qf/ZTqtQTqzwkcF0nRLFmpX79Rs2RlrMDSrboGwfGeV/4THQQZb7hVNWchFwoFGs7H1amomsGFQ+1/+jXjdkZScIWSCwZG1aj3zHH1wqWK6XPiBJZyxmwA8J7ulyMIQZjuuBcViXpe/a//3NlIojgrJ+O+B9M3f679z79hve4ULwWp1selyHNL/3cElk6ZPzXTtc+swKI7LJ0P/b9xViJQdODQKQCQVBVf/gJr6uEC/rjZn/vQPt3aDYQhLTZRnJ0LghCoHxiwBC6cU81ZKDZnDn0XY3JFz+svRKUVAMRN61JplLJ0UdYekSmDNKbRNmskESFIWVklT1P+c/3FFZUzcKXaf+5sVFoBQLiz3X1on3bFOvWCJX07PkjxUuTc+uVQT2dsisSUlWLZzwA6Zf7UNPSZFVhThm7GUu2MxaRSy9NUyN7ds+dtymkDgKJ7vu9pPGs7/HEkm3HhelXR9MbnfwsARfc90nd8lzynTFFQztO0s/qg7fAOACGSU1Uyyzh/DanWMz6Xs+aI4+Q+QeABABNJyr76i/p//8y48BpV0TQAxHF6v/XQR5FSZV/5BSYWAyDtW//ta6kb1MOZy3QzlhJKNeN12k/scdUciaQnr3BsCDzPBfy4QomgqMDzkcTId0EQBrJxHAAgBDG0Bp4Kh1qbxt+o7/Rx0TWbFNPnOD7ZFkmRl1ehpMh76pjA0JEUSV4hAAQaLsRVGKg/r12xTlpQnHof3NVH7Ud3xqaYVt00qrO4otEprgqsKwFZZkH6so0dH/yXcvTiUoUsu5DxpzSBN624qXf/+7bD26XmXPPqzYzX6Tp3HADkOaUZa2617Hoj2Nsp0hoy1t6OoJj92MA/IWvDvZ4LJ/tO7MYlCkHgounnn/4xSpDlX/t1XEPa6UuMC6/p2fNWqLdTYso2r7wFQTHn2YMjVpgi0qJSReV0UXoGJlegpAghCATD4vKEuzul+UXS3IJgS2N/qYJiAKAsnfHVATAu59DEMTTqPXtSv+565bRZjp0fgiAAgHL6oPkgAOBKFQBE17OiRFJwlXrEnkSJk1YAYD8Sn/JZRSkzi0jl1LQVL7BQiUh57XLpnCrcoBFohmrp9H64L3w+ftcJN+qU65eIy4twjVLgec7to5rbA4fPhOv6342S6WXGb93b98/X6NYu9a3XiEvzERynO3q8H+0"
    }
}
```

O retorno consistem em uma imagem no formato base64, para ser adicionada a um html utilize o formato abaixo:

```html
<img src="data:image/png;base64,RETORNO AQUI">
```