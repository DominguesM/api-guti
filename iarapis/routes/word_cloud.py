# coding:utf-8
import base64
import io

from stop_words import get_stop_words
from unidecode import unidecode
from wordcloud import WordCloud

from iarapis import __author__, __version__
from iarapis.routes.base import BaseHandler
from iarapis.schemas import check_schema
from iarapis.schemas.wordcloud_schema import SCHEMA as word_schema
from iarapis.utils.exceptions import NotImplementedError


STOPWORDS = [unidecode(word) for word in get_stop_words("portuguese")]


class WordCloudHandler(BaseHandler):
    async def post(self, *args, **kwargs):
        checked_data = await check_schema(word_schema, self.json_args)

        decode_text = unidecode(checked_data["text"])

        wc = WordCloud(
            stopwords=checked_data.get("stopwords", STOPWORDS),
            max_words=checked_data.get("max_words", 50),
            collocations=False,
        )

        wc_generate = wc.generate(decode_text)

        pil_img = wc_generate.to_image()
        img = io.BytesIO()
        pil_img.save(img, "PNG")
        img.seek(0)
        img_b64 = base64.b64encode(img.getvalue()).decode()

        # img_object = '<img src="data:image/png;base64,{0}">'.format(img_b64)

        self.respond({"img_wc": img_b64})

    async def get(self, *args, **kwargs):
        raise NotImplementedError()
