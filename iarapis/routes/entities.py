# coding:utf-8
import spacy

from iarapis import __author__, __version__
from iarapis.routes.base import BaseHandler
from iarapis.schemas import check_schema
from iarapis.schemas.entity_schema import SCHEMA as ent_schema
from iarapis.utils.exceptions import NotImplementedError


NLP = spacy.load("pt")


class EntitiesHandler(BaseHandler):
    async def post(self, *args, **kwargs):
        data = self.json_args

        checked_data = await check_schema(ent_schema, data)

        nlu_text = NLP(checked_data["text"])

        entities = [{"label": ent.label_, "value": ent.text} for ent in nlu_text.ents]

        self.respond({"entities": entities})

    async def get(self, *args, **kwargs):
        raise NotImplementedError()
