# coding:utf-8
"""
https://web.eecs.umich.edu/~mihalcea/papers/mihalcea.emnlp04.pdf
"""
from copy import copy

from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords

from iarapis import __author__, __version__
from iarapis.routes.base import BaseHandler
from iarapis.schemas import check_schema
from iarapis.schemas.summarization_schema import SCHEMA as summ_schema
from iarapis.utils.exceptions import NotImplementedError


class SummarizationHandler(BaseHandler):
    async def post(self, *args, **kwargs):
        data = self.json_args

        checked_data = await check_schema(summ_schema, data)

        summ_per = summarize(
            checked_data["text"], ratio=checked_data.get("ratio", 0.05), split=True
        )

        highlight_ext = str(copy(checked_data["text"]))

        for summtext in summ_per:
            highlight_ext = highlight_ext.replace(summtext, "<mark>{0}</mark>".format(summtext))

        topics = [keyword for keyword in str(keywords(checked_data["text"])).split("\n")]

        self.respond(
            {
                "summarization": " ".join(summ_per),
                "html_highlight": highlight_ext,
                "topics": topics,
            }
        )

    async def get(self, *args, **kwargs):
        raise NotImplementedError()
