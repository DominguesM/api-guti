# coding: utf-8

import json
import logging
import traceback

import tornado.web
from bson.objectid import ObjectId

from iarapis.utils.exceptions import APIExceptions
from iarapis.utils.handler_utils import smart_parse


logging.basicConfig(
    format=(
        "[%(asctime)s] [%(levelname)8s]" " --- %(message)s (%(filename)s:%(lineno)s)"
    ),
    datefmt="%d/%m/%Y %I:%M:%S %p",
)

logger = logging.getLogger(__name__)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):  # NOQA
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)
        self.json_args = None

    def options(self):
        self.set_status(204)
        self.finish()

    def set_default_headers(self):
        headers = [
            "origin",
            "x-csrf-token",
            "content-type",
            "accept",
            "x-requested-with",
        ]
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", ", ".join(headers))
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")

    def prepare(self):
        logger.debug("Preparing request data")
        self.json_args = smart_parse(self.request.body)

    def write_error(self, status_code, **kwargs):
        exc_info = kwargs["exc_info"]

        if len(exc_info) > 0:
            self.add_header("Content-Type", "application/json")

            info = exc_info[1]
            message = str(info)

            if isinstance(info, APIExceptions):
                self.respond(message, info.code)
            elif isinstance(info, Exception):
                self.respond(message, status_code)
            else:
                logger.error(
                    "\n\n==== IARAPIS SERVER ERROR ====\n%s\n%s\n",
                    __file__,
                    traceback.format_exc(),
                )
                self.respond(
                    "Unknown error. Contact the author for more details.", status_code
                )

    def get(self):
        self.respond({"status": "It works!"})

    def respond(self, data, code=200):
        self.set_header("Content-Type", "application/json")
        self.set_status(code)

        response = {"status": code, "data": data}

        self.write(JSONEncoder().encode(response))

        self.finish()


class StaticFileHandler(tornado.web.StaticFileHandler):
    def set_default_headers(self):
        headers = [
            "origin",
            "x-csrf-token",
            "content-type",
            "accept",
            "x-requested-with",
        ]
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", ", ".join(headers))
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")

    def set_extra_headers(self, path):
        self.set_header("Cache-control", "no-cache")

    def prepare(self):

        if "spec.json" in self.request.uri:
            self.path = "spec.json"

            absolute_path = self.get_absolute_path(self.root, self.path)

            valid_abs_path = self.validate_absolute_path(self.root, absolute_path)

            with open(valid_abs_path) as f:
                swagger_js = json.load(f)

            server_host = self.request.host.split(",")[0]
            forwarded = self.request.headers.get("Forwarded", None)
            proto = None
            if forwarded:
                protopart = [
                    part.strip()
                    for part in forwarded.split(";")
                    if part.strip().startswith("proto")
                ]
                if protopart:
                    proto = protopart[0].split("=")[-1]

            proto = (
                proto
                or self.request.headers.get("X-Forwarded-Proto", None)
                or self.request.protocol
            )
            servers = [
                {
                    "url": proto + "://" + server_host + "/",
                    "description": "Default server",
                }
            ]

            swagger_js["servers"] = servers

            with open(valid_abs_path, "w") as f:
                json.dump(swagger_js, f, indent=4, ensure_ascii=False)
