# coding:utf-8
from iarapis import __author__, __version__
from iarapis.routes.base import BaseHandler
from iarapis.utils.exceptions import NotImplementedError


class HealthHandler(BaseHandler):
    async def post(self, *args, **kwargs):
        raise NotImplementedError()

    async def get(self, *args, **kwargs):

        self.respond({"version": __version__, "author": __author__})
