# coding:utf-8

import json
import logging
from urllib.parse import parse_qs as form_urlencoded_parse


logger = logging.getLogger(__name__)


def smart_parse(body):
    """
    Handle json, fall back to x-www-form-urlencoded
    """
    try:
        data_dict = json.loads(body)
    except ValueError:
        data_dict = form_urlencoded_parse(body)
    except BaseException:

        logger.warning("Request body is empty or contains formatting errors")
        return dict()

    return data_dict
