# coding:utf-8


class APIExceptions(Exception):
    def __init__(self, message, code=500):
        self.message = message
        self.code = code
        super(Exception, self).__init__(message)


class NotImplementedError(APIExceptions):
    def __init__(self):
        APIExceptions.__init__(self, "Not Implemented", 501)


class MissingField(APIExceptions):
    def __init__(self, field):
        APIExceptions.__init__(self, "%s field was not provided" % field, 400)


class WrongFieldType(APIExceptions):
    def __init__(self, field, _given, _required):
        APIExceptions.__init__(
            self,
            "Field %s - %s is type %s but should be %s"
            % (field, _given, type(_given), _required),
            400,
        )
