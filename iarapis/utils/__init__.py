import logging
import time
from functools import wraps

logger = logging.getLogger(__name__)


def unicode(s):
    return str(s)


def timed(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        logger.info("%s ran in %ss", str(func.__name__), str(round(end - start, 2)))
        return result

    return wrapper
