import logging
import sys
import os

root = logging.getLogger()
data_dir = "/data/log"
filename = "service-log.log"
file = data_dir + "/" + filename if os.path.exists(data_dir) else filename
file_handler = logging.FileHandler(filename=file)
stdout_handler = logging.StreamHandler(sys.stdout)

formatter = logging.Formatter(
    "[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s"
)

file_handler.setFormatter(formatter)
stdout_handler.setFormatter(formatter)

root.setLevel(logging.INFO)
root.addHandler(file_handler)
root.addHandler(stdout_handler)
root.propagate = False

print(os.environ)

URL_PRE = os.environ.get(
    "URL_PRE", "http://54.208.22.251:8886/api/text-preprocessing/v1.0/pipeline"
)

MONGO_URI = os.environ.get("MONGO_URI", "mongodb://localhost:27017")

SG_CLIENTE = os.environ.get("SG_CLIENTE", "pgesp")

root.info(MONGO_URI)
root.info(SG_CLIENTE)

os.environ["ASYNC_TEST_TIMEOUT"] = "600"
