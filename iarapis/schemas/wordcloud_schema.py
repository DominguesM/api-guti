SCHEMA = {
    "text": (str, True, False),
    "max_words": (int, False, False),
    "stopwords": (list, False, False),
}
