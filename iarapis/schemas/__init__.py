# coding:utf-8
"""
check_schema

Example:

    SCHEMA = {
        "url": (str, True, False)
    }

Parameters:

    str | True | False

    str     : Format of the data to be validated
    True    : Required field
    False   : If the data is not mandatory and not present in the input data create the field in the output data.

"""

from iarapis.utils.exceptions import MissingField, WrongFieldType


async def check_schema(schema: dict, data: dict) -> dict:
    """
    Check Schema

    A function for data shape definition and validation

    Args:
        schema: Validation Schema (dict)
        data: Input data to be validated (dict)

    Returns:
        Formatted and validated data.

    Raises:
        WrongFieldType: 400
        MissingField: 400
    """
    if not isinstance(data, dict):
        raise WrongFieldType("Schema", data, dict)

    for key in schema:
        if key in data:
            if not isinstance(data[key], schema[key][0]):
                unicode_exc = schema[key][0] is str and isinstance(data[key], str)
                if not unicode_exc:
                    raise WrongFieldType(key, data[key], schema[key][0])
        elif schema[key][1]:
            raise MissingField(key)
        elif schema[key][2]:
            data[key] = schema[key][0]()

    return data


async def check_list_is_int(data: list) -> bool:
    return all(isinstance(n, int) for n in data)
