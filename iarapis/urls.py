# -*- coding: utf-8 -*-
import os

from iarapis import __version__
from iarapis.routes import base, health, summarization, entities, word_cloud

BASE_URL = "/tools/v{}".format(__version__)

url_patterns = [
    (r"/", base.BaseHandler),
    (r"{0}/health".format(BASE_URL), health.HealthHandler),
    (r"{0}/summarization".format(BASE_URL), summarization.SummarizationHandler),
    (r"{0}/entities".format(BASE_URL), entities.EntitiesHandler),
    (r"{0}/wordcloud".format(BASE_URL), word_cloud.WordCloudHandler),
]
